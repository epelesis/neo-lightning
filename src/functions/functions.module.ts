import { Module } from '@nestjs/common';
import { FunctionsService } from './functions.service';
import { FunctionsResolver } from './functions.resolver';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Function } from './entities/function.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Function])],
  providers: [FunctionsResolver, FunctionsService],
})
export class FunctionsModule {}
