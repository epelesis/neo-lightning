import { Resolver, Query, Mutation, Args, Int } from '@nestjs/graphql';
import { FunctionsService } from './functions.service';
import { Function } from './entities/function.entity';
import { CreateFunctionInput } from './dto/create-function.input';
import { UpdateFunctionInput } from './dto/update-function.input';

@Resolver(() => Function)
export class FunctionsResolver {
  constructor(private readonly functionsService: FunctionsService) {}

  @Mutation(() => Function)
  createFunction(
    @Args('createFunctionInput') createFunctionInput: CreateFunctionInput,
  ) {
    return this.functionsService.create(createFunctionInput);
  }

  @Query(() => [Function], { name: 'functions' })
  findAll() {
    return this.functionsService.findAll();
  }

  @Query(() => Function, { name: 'function' })
  findOne(@Args('id', { type: () => Int }) id: number) {
    return this.functionsService.findOne(id);
  }

  @Mutation(() => Function)
  updateFunction(
    @Args('updateFunctionInput') updateFunctionInput: UpdateFunctionInput,
  ) {
    return this.functionsService.update(
      updateFunctionInput.id,
      updateFunctionInput,
    );
  }

  @Mutation(() => Function)
  removeFunction(@Args('id', { type: () => Int }) id: number) {
    return this.functionsService.remove(id);
  }
}
