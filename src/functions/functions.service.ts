import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Function } from './entities/function.entity';
import { CreateFunctionInput } from './dto/create-function.input';
import { UpdateFunctionInput } from './dto/update-function.input';

@Injectable()
export class FunctionsService {
  constructor(
    @InjectRepository(Function)
    private functionRepository: Repository<Function>,
  ) {}

  create(createFunctionInput: CreateFunctionInput): Promise<Function> {
    const createdFunction = new Function();
    createdFunction.name = createFunctionInput.name;
    createdFunction.namespace = createFunctionInput.namespace;

    const body = Buffer.from(
      createFunctionInput.encoded_body,
      'base64',
    ).toString('utf8');

    createdFunction.body = body;
    return this.functionRepository.save(createdFunction);
  }

  async findAll(): Promise<Function[]> {
    return this.functionRepository.find();
  }

  async findOne(id: number): Promise<Function | undefined> {
    return this.functionRepository.findOne(id);
  }

  async remove(id: number) {
    return this.functionRepository.delete(id);
  }

  async update(id: number, updateFunctionInput: UpdateFunctionInput) {
    return `This action updates a #${id} function`;
  }
}
