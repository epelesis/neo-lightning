import { Field, Int, ObjectType } from '@nestjs/graphql';
import { Process } from 'src/processes/entities/process.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';

@Entity()
@ObjectType()
export class Function {
  @PrimaryGeneratedColumn()
  @Field((type) => Int)
  id!: number;

  @Column()
  @Field()
  name!: string;

  @Column()
  @Field()
  namespace!: string;

  @Column()
  @Field()
  body!: string;

  @OneToMany((type) => Process, (process) => process.function)
  processes!: Process[];
}
