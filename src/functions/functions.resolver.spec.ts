import { Test, TestingModule } from '@nestjs/testing';
import { FunctionsResolver } from './functions.resolver';
import { FunctionsService } from './functions.service';

describe('FunctionsResolver', () => {
  let resolver: FunctionsResolver;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FunctionsResolver, FunctionsService],
    }).compile();

    resolver = module.get<FunctionsResolver>(FunctionsResolver);
  });

  it('should be defined', () => {
    expect(resolver).toBeDefined();
  });
});
