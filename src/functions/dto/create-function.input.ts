import { InputType, Field } from '@nestjs/graphql';

@InputType()
export class CreateFunctionInput {
  @Field({ description: 'Function Name (Including Extension' })
  name: string;

  @Field({ description: 'Namespace of Function)' })
  namespace: string;

  @Field({ description: 'Function Body (Base 64 Encoded)' })
  encoded_body: string;
}
