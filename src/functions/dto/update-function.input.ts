import { CreateFunctionInput } from './create-function.input';
import { InputType, Field, Int, PartialType } from '@nestjs/graphql';

@InputType()
export class UpdateFunctionInput extends PartialType(CreateFunctionInput) {
  @Field(() => Int)
  id: number;
}
