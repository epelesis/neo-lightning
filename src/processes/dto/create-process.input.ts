import { InputType, Int, Field } from '@nestjs/graphql';

@InputType()
export class CreateProcessInput {
  @Field({ description: 'Process Entrypoint' })
  entrypoint: string;

  @Field({ description: 'Process Arguments' })
  arguments: string[];

  @Field({ description: 'Process Function ID' })
  function: string;
}
