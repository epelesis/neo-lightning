import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateProcessInput } from './dto/create-process.input';
import { UpdateProcessInput } from './dto/update-process.input';
import { Process } from './entities/process.entity';

@Injectable()
export class ProcessesService {
  constructor(
    @InjectRepository(Process)
    private processRepository: Repository<Process>,
  ) {}

  create(createProcessInput: CreateProcessInput) {
    const createdProcess = new Process();

    return this.processRepository.save(createdProcess);
  }

  async findAll(): Promise<Process[]> {
    return this.processRepository.find();
  }

  async findOne(id: number): Promise<Process | undefined> {
    return this.processRepository.findOne(id);
  }

  async remove(id: number) {
    return this.processRepository.delete(id);
  }

  async update(id: number, updateProcessInput: UpdateProcessInput) {
    return `This action updates a #${id} process`;
  }
}
