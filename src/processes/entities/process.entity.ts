import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import { Function } from 'src/functions/entities/function.entity';
import { Field, Int, ObjectType } from '@nestjs/graphql';
import { NotImplementedException } from '@nestjs/common';

export enum ProcessState {
  Pending = 'Pending',
  Running = 'Running',
  Halted = 'Halted',
}

@Entity()
@ObjectType()
export class Process {
  @PrimaryGeneratedColumn()
  @Field((type) => Int)
  id: number;

  @Column()
  @Field()
  state!: ProcessState;

  @Column()
  @Field()
  entrypoint!: string;

  @Column('text', { array: true })
  @Field()
  arguments!: string[];

  @Column()
  @Field()
  output!: string;

  @ManyToOne(() => Function, (fn) => fn.processes)
  @Field((type) => Int)
  function!: Function;
}
