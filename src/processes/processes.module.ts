import { Module } from '@nestjs/common';
import { ProcessesService } from './processes.service';
import { ProcessesResolver } from './processes.resolver';
import { Process } from './entities/process.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Process])],
  providers: [ProcessesResolver, ProcessesService],
})
export class ProcessesModule {}
