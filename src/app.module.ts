import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { FunctionsModule } from './functions/functions.module';
import { ProcessesModule } from './processes/processes.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      sortSchema: true,
    }),
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'dev.db',
      synchronize: true,
      autoLoadEntities: true,
    }),
    FunctionsModule,
    ProcessesModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
